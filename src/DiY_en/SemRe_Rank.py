import re
import itertools
import nltk
import random
import networkx as nx
from nltk.stem import WordNetLemmatizer
import io
from scipy.spatial import distance
import operator
import pandas as pd
import numpy as np

from sklearn.metrics import average_precision_score, roc_auc_score, roc_curve


def createstopword_dic():
    '''
    Generate list of stop words
    :return:
    '''
    stopWordList = []
    stopWords = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/stopwordlist.txt','r')
    for word in stopWords:
        stopWordList.append(word.strip('\n'))
    return stopWordList


def load_goldlabels(stopword):
    '''
    Generate the gold data dictionary
    :param stopword:
    :return:
    '''
    golddata = {}
    with open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/input/goldStandard_diy.txt','r') as file:
        for line in file.readlines():
            word = line.split('\t')
            if word[0] not in stopword:
                golddata[word[0]] = int(word[1].strip())

    return golddata

def preProcessing(stopwords):
    '''
    Preprocess method removes the stopwords, alphanumerical words, cooccurrence value less than 3, words length less than 3 letters.
    :param stopwords:
    :return:
    '''

    data = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/input/DIY_corpus_pmi.txt', 'r')
    writeData = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/input/DIY_corpus_pmi.csv', 'w+')
    i = 0
    for line in data.readlines():
        word = line.split('\t')
        word[0] = re.sub('[.@]', '', word[0])
        word[1] = re.sub('[.@]', '', word[1])

        if (word[0] in stopwords or word[1] in stopwords):
            continue
        elif (not (word[0].isalpha() or word[1].isalpha())):
            continue
        elif (len(word[0]) < 3 or len(word[1]) < 3):
            continue
        elif (word[2].strip() == '1') or (word[2].strip() == '2'):
            continue
        else:
            writeData.write(word[0] + "\t")
            writeData.write(word[1] + "\t")
            writeData.write(word[2]+'\n')
            i+=1
    print('number of lines:',i)


def load_vectors():
    '''
    Load the cosine vector from modified Vector file filtered the unused word
    :return:
    '''

    fin = io.open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/input/modifiedVectorFile.csv', 'r', newline='\n', errors='ignore')
    vectors = {}
    i = 1
    for line in fin:
        tokens = line.rstrip().split(' ')
        vectors[tokens[0]] = list(map(float, tokens[1:]))
        i += 1
    print(i)
    return vectors

def shuffle_seed():
    '''
    Shuffle the seed terms from he gold Standard
    :return:
    '''
    fid = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/input/goldStandard_diy.txt','r')
    li = fid.readlines()
    fid.close()

    random.shuffle(li)

    fid = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/input/goldStandard_diy_shuffled.txt','w+')
    fid.writelines(li)
    fid.close()

def seedterms():
    '''
    Creating the seedterm dictionary
    :return:
    '''
    seedtermsdict = {}
    shuffle_seed()
    with open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/input/goldStandard_diy_shuffled.txt','r') as file:
        for line in itertools.islice(file,0,100):
            word = line.split('	')
            seedtermsdict[word[0]] = int(word[1].strip('\n'))

    return seedtermsdict

def calculatepagerank():
    '''
    Calculate the page rank
    :return:
    '''
    data = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/input/DIY_corpus_lmi.csv', 'r')
    #personalizedDict = seedterms()
    mainList = []
    mainWord = {}
    firstWord = ''

    for line in data.readlines():
        secondDict = {}
        cols = line.split('\t')
        if (firstWord != cols[0]):
            mainList = []
            secondDict[cols[1]] = cols[2].strip()
            firstWord = cols[0]
            mainList.append(secondDict)

        else:
            secondDict[cols[1]] = cols[2].strip()
            mainList.append(secondDict)

        mainWord[firstWord] = mainList


    D = nx.DiGraph()

    #vectors = load_vectors()
    for key, value in mainWord.items():

        for edge in value:
            for k, v in edge.items():
                #dist = distance.cosine(vectors[key],vectors[k])
                #print(dist)
                # print('key:',data[key],' k:',data[k],' distance:',dist)
                D.add_edge(key, k, distance=v)

    #rank = nx.pagerank(D, alpha=0.8,  personalization = personalizedDict)
    rank = nx.pagerank(D, alpha=0.85)

    writeData = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/output/LMI/Without_seedterms/DIY_LMI_pagerank_without_seed.csv', 'w+')

    for key, value in rank.items():
        writeData.write(key + "\t")
        writeData.write(str(round(value, 6)))
        writeData.write("\n")

    writeData.close()

    #return personalizedDict

def sorted():
    '''
    Sort the page rank file
    :return:
    '''

    df = pd.read_csv('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/output/LMI/Without_seedterms/DIY_LMI_pagerank_without_seed.csv', delimiter="\t", header=None)
    df = df.sort_values(1,ascending = False)
    df.to_csv('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/output/LMI/Without_seedterms/DIY_LMI_pagerank_without_seed_sorted.csv', index=False, header=None, sep="\t")

def sorted_metric():
    '''
    Sort the evaluation file
    :return:
    '''
    df = pd.read_csv('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/output/LMI/Without_seedterms/evaluation_data_without_seed.csv',delimiter="\t", header=None)
    df = df.sort_values(1, ascending=False)
    df.to_csv('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/output/LMI/Without_seedterms/evaluation_data_without_seed_sorted.csv',index=False, header=None, sep="\t")


def evaluation(goldlabeldict):
    '''
    Evaluating the sorting file
    :param goldlabeldict:
    :param seedterm:
    :return:
    '''
    #goldlabeldict = {}
    pagerankdict = {}

    pagerank = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/output/LMI/Without_seedterms/DIY_LMI_pagerank_without_seed_sorted.csv','r')

    #items = list(goldlabeldict.items())

    for line in pagerank.readlines():
        word = line.split('\t')
        pagerankdict[word[0].strip()] = word[1].strip()
    # create file with word pagerank gold label
    averageprecisiondict = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/output/LMI/Without_seedterms/evaluation_data_without_seed.csv','w+')
    for key,value in goldlabeldict.items():
        if (key in pagerankdict.keys()):
        #if (key in pagerankdict.keys()) and (key not in seedterm.keys()):
            averageprecisiondict.write(key+"\t")
            averageprecisiondict.write(pagerankdict[key]+"\t")
            averageprecisiondict.write(str(value))
            averageprecisiondict.write("\n")

    averageprecisiondict.close()

    # Sorted the evaluation file with respect to page rank value
    sorted_metric()

    y_true = []
    y_score = []
    data = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/DiY_en/FirstOrderCooccurrence/output/LMI/Without_seedterms/evaluation_data_without_seed_sorted.csv','r')

    for line in data.readlines():
        word = line.split("\t")
        y_score.append(float(word[1].strip()))
        y_true.append(int(word[2].strip()))

    print(y_score)
    print(y_true)
    avgprecision = average_precision_score(np.array(y_true),np.array(y_score))
    rocauc_score = roc_auc_score(np.array(y_true),np.array(y_score))

    print("AP: %0.2f, ROC_score: %0.2f" % (avgprecision, rocauc_score))


def main():

    stopword = createstopword_dic()

    #preProcessing(stopword)

    calculatepagerank()
    # sort the page rank value generated in the file
    sorted()

    goldlables = load_goldlabels(stopword)

    evaluation(goldlables)

    
if __name__ == "__main__":
    main()