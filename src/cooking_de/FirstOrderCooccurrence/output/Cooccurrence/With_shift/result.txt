
Default:
AP: 0.67, ROC_score: 0.69

With Baseline:

baseline 1 AP: 0.48, ROC_score: 0.51
baseline 2 AP: 0.48, ROC_score: 0.52
baseline 3 AP: 0.47, ROC_score: 0.48

Average_AP: 0.48, Average_ROC_score: 0.50