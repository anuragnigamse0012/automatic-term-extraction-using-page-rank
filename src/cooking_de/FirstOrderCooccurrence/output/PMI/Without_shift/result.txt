
Default:

AP: 0.57, ROC_score: 0.59



With Baseline:

baseline 1 AP: 0.48, ROC_score: 0.49
baseline 2 AP: 0.48, ROC_score: 0.50
baseline 3 AP: 0.50, ROC_score: 0.51

Average_AP: 0.49, Average_ROC_score: 0.50