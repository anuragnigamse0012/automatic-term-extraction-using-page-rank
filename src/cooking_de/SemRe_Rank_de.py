import re
import os
import itertools
import random
import sys
import nltk
import networkx as nx
from nltk.stem import WordNetLemmatizer
import io
from scipy.spatial import distance

import pandas as pd
import numpy as np

from sklearn.metrics import average_precision_score, roc_auc_score, roc_curve


def createstopword_dic():
    '''
    Create a stop word list
    :return:
    '''
    stopWordList = []

    with open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/stopwords-de.txt','r') as file:
        for word in file.readlines():
            stopWordList.append(word.strip('\n'))
    return stopWordList




def checkNumbers(input):
    '''
    Checks if the input(words) contain numerical value
    :param input:
    :return:
    '''
    return any(char.isdigit() for char in input)

def checkSpecialCharacter(input):
    '''
    Checks if the the word contain special characters as - or |
    :param input:
    :return:
    '''
    return bool(re.search(r'[-|%*/=]', input))

def preprocessing(path,stopwords):
    '''
    Pre-processed the input data
    :param path:
    :return:
    '''
    data = open(os.path.join(path,'FirstOrderCooccurrence/extras/cooking_corpus_window_20.txt'),'r')
    writeData = open(os.path.join(path,'FirstOrderCooccurrence/input/cooking_corpus.csv'), 'w+')
    i = 0

    for line in data.readlines():
        word = line.split('\t')
        word[0] = re.sub('[.@]', '', word[0])
        word[1] = re.sub('[.@]', '', word[1])

        if (word[0] in stopwords or word[1] in stopwords):
            continue
        elif (not (word[0].isalpha() or word[1].isalpha())):
            continue
        elif (len(word[0]) < 3 or len(word[1]) < 3):
            continue
        elif (checkNumbers(word[0]) or checkNumbers(word[1])):
            continue
        elif (checkSpecialCharacter(word[0])) or checkSpecialCharacter(word[1]):
            continue
        elif (word[2].strip() == '1') or (word[2].strip() == '2'):
            continue
        else:
            writeData.write(word[0] + "\t")
            writeData.write(word[1] + "\t")
            writeData.write(word[2])
            i += 1
    print('number of lines:', i)
    writeData.close()



def load_goldlabels(path,stopword):
    '''
    create dictionary for gold labels
    :param path:
    :param stopword:
    :return:
    '''
    golddata = {}
    with open(os.path.join(path,'FirstOrderCooccurrence/input/cooking_gold_standard.txt'),'r') as file:
        for line in file.readlines():
            word = line.split('\t')
            if word[0] not in stopword:
                golddata[word[0]] = int(word[1].strip())

    return golddata


def seedterms(path):
    '''
    cooking shift values
    :param path:
    :return:
    '''
    seedtermsdict = {}
    with open(os.path.join(path,'input/cooking_shift_values.txt'),
              'r') as file:
        for line in file.readlines():
            word = line.split('	')
            seedtermsdict[word[0]] = float(word[1].strip('\n'))

    return seedtermsdict


def load_vectors(path):
    '''
    load vector from the cosine vector file
    :param path:
    :return:
    '''
    fin = io.open(os.path.join(path,'input/modifiedVectorFile.csv'), 'r', newline='\n', errors='ignore')
    vectors = {}
    i = 1
    for line in fin:
        tokens = line.rstrip().split(' ')
        vectors[tokens[0]] = list(map(float, tokens[1:]))
        i += 1
    print(i)
    return vectors

# Create word vector filter file
def createFilteredVectorFile(mainWords):
  data = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/cooking_de/data/input/cc.de.300.vec','r')
  reviseData = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/cooking_de/data/output/modifiedVectorFile.csv','w+')
  for line in data.readlines():
    cols = line.split(" ",1)[0]
    if((cols.strip()) in mainWords):
      reviseData.write(line)


def calculatepagerank(path):
    '''
    Calculate page rank
    :param path:
    :return:
    '''

    mainKey = []
    personalizedDict = {}




    data = open(os.path.join(path,'SecondOrderCooccurrence/input/cooking_corpus_second_order.csv'), 'r')
    mainList = []
    mainWord = {}
    firstWord = ''

    for line in data.readlines():

        secondDict = {}
        cols = line.split('\t')
        if (firstWord != cols[0]):
            mainList = []
            secondDict[cols[1]] = cols[2].strip()
            firstWord = cols[0]
            mainList.append(secondDict)

        else:
            secondDict[cols[1]] = cols[2].strip()
            mainList.append(secondDict)

        mainWord[firstWord] = mainList

    for k, v in mainWord.items():
        mainKey.append(k)

    '''shiftDict = seedterms(path)
    # generate the personalised dictionary
    for value in mainKey:
        if value in shiftDict:
            personalizedDict[value] = float(abs(1 - float(shiftDict[value])))
        else:
            personalizedDict[value] = 0'''

    D = nx.DiGraph()
    #vectors = load_vectors(path)
    for key, value in mainWord.items():

        for edge in value:
            for k, v in edge.items():
                #if key in vectors and k in vectors:
                    #dist = distance.cosine(vectors[key],vectors[k])
                    D.add_edge(key, k, distance=v)

    #rank = nx.pagerank(D, alpha=0.85, personalization=personalizedDict)
    rank = nx.pagerank(D, alpha=0.85)

    writeData = open(os.path.join(path,'SecondOrderCooccurrence/output/Cooccurrence/Without_shift/cooking_cooccurrence_pagerank_without_shift.csv'),'w+')

    for key, value in rank.items():
        writeData.write(key + "\t")
        writeData.write(str(round(value, 6)))
        writeData.write("\n")

    writeData.close()


def sorted(path):
    df = pd.read_csv(os.path.join(path,'SecondOrderCooccurrence/output/Cooccurrence/Without_shift/cooking_cooccurrence_pagerank_without_shift.csv'),delimiter="\t", header=None)
    df = df.sort_values(1, ascending=False)
    df.to_csv(os.path.join(path,'SecondOrderCooccurrence/output/Cooccurrence/Without_shift/cooking_cooccurrence_pagerank_without_shift_sorted.csv'),
        index=False, header=None, sep="\t")


def sorted_metric(path):
    df = pd.read_csv(os.path.join(path,'SecondOrderCooccurrence/output/Cooccurrence/Without_shift/cooking_evaluation_without_shift.csv'),delimiter="\t", header=None)
    df = df.sort_values(1, ascending=False)
    df.to_csv(os.path.join(path,'SecondOrderCooccurrence/output/Cooccurrence/Without_shift/cooking_evaluation_without_shift_sorted.csv'), index=False, header=None, sep="\t")



def evaluation(path,goldlabeldict):
    '''
    Evaluation of the page rank value
    :param path:
    :param goldlabeldict:
    :return:
    '''

    pagerankdict = {}

    with open(os.path.join(path, 'SecondOrderCooccurrence/output/Cooccurrence/Without_shift/cooking_cooccurrence_pagerank_without_shift_sorted.csv'), 'r') as file:
        for line in file.readlines():
            word = line.split('\t')
            pagerankdict[word[0].strip()] = word[1].strip()

    # create file with word pagerank and gold label
        averageprecisiondict = open(os.path.join(path,'SecondOrderCooccurrence/output/Cooccurrence/Without_shift/cooking_evaluation_without_shift.csv'),'w+')
        for key, value in goldlabeldict.items():
            if (key in pagerankdict.keys()):
                averageprecisiondict.write(key+'\t')
                averageprecisiondict.write(pagerankdict[key] + '\t')
                averageprecisiondict.write(str(value)+'\n')

        averageprecisiondict.close()

        # Sorted the evaluation file with respect to page rank value
        sorted_metric(path)

        y_true = []
        y_score = []
        with open(os.path.join(path, 'SecondOrderCooccurrence/output/Cooccurrence/Without_shift/cooking_evaluation_without_shift_sorted.csv'), 'r') as file:

            for line in file.readlines():
                word = line.split("\t")
                y_score.append(float(word[1].strip()))
                y_true.append(int(word[2].strip('\n')))


        print(y_score)
        print(y_true)

        avgprecision = average_precision_score(np.array(y_true), np.array(y_score))
        rocauc_score = roc_auc_score(np.array(y_true), np.array(y_score))
        print("AP: %0.2f, ROC_score: %0.2f" % (avgprecision,rocauc_score))


if __name__ == "__main__":
   path = os.getcwd() # /home/anurag/Documents/automatic-term-extraction-using-page-rank/src/cooking_de

   try:
       os.path.exists(path)
   except:
       print(sys.stderr)

   print(path)

   stopword = createstopword_dic()


   #preprocessing(path,stopword)

   calculatepagerank(path)

   #sorted(path)

   #goldlabel = load_goldlabels(path,stopword)

   #evaluation(path,goldlabel)

