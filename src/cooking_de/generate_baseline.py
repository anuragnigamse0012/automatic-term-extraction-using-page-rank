import random
import os
import numpy as np

from sklearn.metrics import average_precision_score, roc_auc_score, roc_curve


def sorted_list(path):

    dict = {}
    sorted_words = []
    with open(os.path.join(path,'FirstOrderCooccurrence/output/Cooccurrence/Without_shift/cooking_evaluation_without_shift_sorted.csv'),'r') as file:
        for line in file.readlines():
            word = line.split('\t')
            dict[word[0].strip()] = word[2].strip()
            sorted_words.append(word[0].strip())

    return sorted_words, dict


def create_evaluate(path,words,gold_dict):
    avg_ap = []
    avg_roc = []
    for i in range(3):
        word_dic = {}
        base = open(os.path.join(path,'FirstOrderCooccurrence/output/Cooccurrence/Without_shift/baseline_%d_without_shift_sorted.csv' %(i+1)),'w+')
        sampled_list = random.sample(words, len(words))
        j = 1
        for word in sampled_list:
            word_dic[word.strip()] = j
            j += 1

        for word in words:
            if word in word_dic.keys():
                base.write(word.strip()+'\t')
                base.write(gold_dict[word].strip()+'\t')
                base.write(str(word_dic[word])+'\n')
            else:
                print('not there',word)
        base.close()

        #evaluate the AP for baseline data
        y_true = []
        y_score = []
        with open(os.path.join(path, 'FirstOrderCooccurrence/output/Cooccurrence/Without_shift/baseline_%d_without_shift_sorted.csv'%(i+1)), 'r') as file:

            for line in file.readlines():
                word = line.split("\t")
                y_true.append(int(word[1].strip()))
                y_score.append(int(word[2].strip('\n')))

        avgprecision = average_precision_score(np.array(y_true), np.array(y_score))
        avg_ap.append(avgprecision)
        rocauc_score = roc_auc_score(np.array(y_true), np.array(y_score))
        avg_roc.append(rocauc_score)
        print("baseline %d AP: %0.2f, ROC_score: %0.2f" % (i+1,avgprecision, rocauc_score))

    return sum(avg_ap)/(i+1),sum(avg_roc)/(i+1)


if __name__ == '__main__':
    path = os.getcwd()
    words,gold_dict = sorted_list(path)
    ap,roc = create_evaluate(path,words, gold_dict)
    print("Average_AP: %0.2f, Average_ROC_score: %0.2f" % (ap, roc))







