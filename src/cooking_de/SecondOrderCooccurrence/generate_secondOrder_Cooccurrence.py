import pandas as pd
import numpy as np


def calculatepagerank():
    data = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/cooking_de/FirstOrderCooccurrence/input/cooking_corpus.csv', 'r')
    #personalizedDict = seedterms()
    mainList = []
    mainWord = {}
    firstWord = ''

    for line in data.readlines():
        secondDict = {}
        cols = line.split('\t')
        if (firstWord != cols[0]):
            mainList = []
            secondDict[cols[1]] = cols[2].strip()
            firstWord = cols[0]
            mainList.append(secondDict)

        else:
            secondDict[cols[1]] = cols[2].strip()
            mainList.append(secondDict)

        mainWord[firstWord] = mainList

    return mainWord


def create_Only_SecondOrderOccurrence(firstOrder):
    secondOrder = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/cooking_de/SecondOrderCooccuurence/input/cooking_corpus_second_order.csv', 'w+')
    for key,value in firstOrder.items():
        firstWord = key
        first_list = value
        for cooccur_word in first_list:
            for k,v in cooccur_word.items():
                second_list = firstOrder[k]
                for second in second_list:
                    for i,j in second.items():
                        secondOrder.write(firstWord+"\t")
                        secondOrder.write(i.strip()+"\t")
                        secondOrder.write(str(int(v)+int(j))+"\n")
                        second_list = []
    secondOrder.close()



if __name__ == '__main__':

    firstOrder = calculatepagerank()
    create_Only_SecondOrderOccurrence(firstOrder)