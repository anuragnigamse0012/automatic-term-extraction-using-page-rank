import pandas as pd
import numpy as np


def calculatepagerank():
    data = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/SecondOrderCooccurrence/test_2.csv', 'r')
    #personalizedDict = seedterms()
    mainList = []
    mainWord = {}
    firstWord = ''

    for line in data.readlines():
        secondDict = {}
        cols = line.split('\t')
        if (firstWord != cols[0]):
            mainList = []
            secondDict[cols[1]] = cols[2].strip()
            firstWord = cols[0]
            mainList.append(secondDict)

        else:
            secondDict[cols[1]] = cols[2].strip()
            mainList.append(secondDict)

        mainWord[firstWord] = mainList

    for k,v in mainWord.items():
        print(k,v)
    return mainWord

def get_list(cooccur_list):
    list_1 = []
    for i in cooccur_list:
        for key,value in i.items():
            list_1.append(key)
    return list_1

def get_second_order(first_list,second_list,key):
    value = 0
    status = False
    threshold = 2
    first_list = get_list(first_list)
    second_list = get_list(second_list)

    for word in first_list:

        if word in second_list and word != key:
            value += 1

    if value >= threshold:
        status = True

    return status


def createSecondOrderOccurrence(firstOrder):
    secondOrder = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/SecondOrderCooccurrence/DIY_test.csv', 'w+')
    for key,value in firstOrder.items():
        firstWord = key
        first_list = value
        for cooccur_word in first_list:
            for k,v in cooccur_word.items():
                second_list = firstOrder[k]
                get_status = get_second_order(first_list,second_list,k)
                if get_status == True:
                    secondOrder.write(firstWord+"\t")
                    secondOrder.write(k+"\t")
                    secondOrder.write(v+"\n")
                    second_list = []
                else:
                    continue

    secondOrder.close()



def create_Only_SecondOrderOccurrence(firstOrder):
    secondOrder = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/SecondOrderCooccurrence/DIY_test.csv', 'w+')
    for key,value in firstOrder.items():
        firstWord = key
        first_list = value
        for cooccur_word in first_list:
            for k,v in cooccur_word.items():
                second_list = firstOrder[k]
                for second in second_list:
                    for i,j in second.items():
                        secondOrder.write(firstWord+"\t")
                        secondOrder.write(i.strip()+"\t")
                        secondOrder.write(str(int(v)+int(j))+"\n")
                        second_list = []
    secondOrder.close()





firstOrder = calculatepagerank()
'''for k,v in firstOrder.items():
    print(k,v)'''
createSecondOrderOccurrence_only(firstOrder)