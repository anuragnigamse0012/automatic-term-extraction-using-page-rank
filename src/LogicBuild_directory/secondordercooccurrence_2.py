import pandas as pd
import numpy as np


def calculatepagerank():
    data = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/SecondOrderCooccurrence/test_2.csv', 'r')
    #personalizedDict = seedterms()
    mainList = []
    mainWord = {}
    firstWord = ''

    for line in data.readlines():
        secondDict = {}
        cols = line.split('\t')
        if (firstWord != cols[0]):
            mainList = []
            secondDict[cols[1]] = cols[2].strip()
            firstWord = cols[0]
            mainList.append(secondDict)

        else:
            secondDict[cols[1]] = cols[2].strip()
            mainList.append(secondDict)

        mainWord[firstWord] = mainList

    for k,v in mainWord.items():
        print(k,v)
    return mainWord

def get_list(cooccur_list):
    list_1 = []
    for i in cooccur_list:
        for key,value in i.items():
            list_1.append(key)
    return list_1

def get_second_order(v,second_list):
    second_order_dic = {}
    for i in second_list:
        for key,value in i.items():
            second_order_dic[key] = int(v.strip()) + int(value.strip())
    return second_order_dic


def createSecondOrderOccurrence(firstOrder):
    secondOrder = open('/home/anurag/Documents/automatic-term-extraction-using-page-rank/src/SecondOrderCooccurrence/DIY_test_2.csv', 'w+')
    for key,value in firstOrder.items():
        firstWord = key
        first_list = value
        for cooccur_word in first_list:
            for k,v in cooccur_word.items():
                secondOrder.write(key + "\t")
                secondOrder.write(k + "\t")
                secondOrder.write(v + "\n")

                second_list = firstOrder[k]
                get_secondOrder_dic = get_second_order(v,second_list)
                for i,j in get_secondOrder_dic.items():
                    secondOrder.write(key+"\t")
                    secondOrder.write(i+"\t")
                    secondOrder.write(str(j)+"\n")

    secondOrder.close()







firstOrder = calculatepagerank()
'''for k,v in firstOrder.items():
    print(k,v)'''
createSecondOrderOccurrence(firstOrder)